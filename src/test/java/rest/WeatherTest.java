package rest;

import kong.unirest.*;
import kong.unirest.json.JSONObject;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;

public class WeatherTest {

    private final HttpResponse<JsonNode> response;
    private final JSONObject json;

    {
        Properties prop = new Properties();

        try (InputStream is = new FileInputStream("src/test/resources/weatherTest.properties")) {
            prop.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.response = Unirest
                .get("https://api.weather.yandex.ru/v1/forecast?lat=55.75396&lon=37.620393&limit=2")
                .header("X-Yandex-API-Key", prop.getProperty("yandex-weather-api-key"))
                .asJson();

        this.json = this.response.getBody().getObject();
    }

    /**
     * К стандартному времени UTC(+0) прибавляется offset населенного пункта.
     *
     * @return локальное время населенного пункта
     */
    private int getZoneTime() {
        return ZonedDateTime.now(
                ZoneOffset.ofTotalSeconds(
                        this.json.getJSONObject("info")
                                .getJSONObject("tzinfo")
                                .getInt("offset")))
                .getMonthValue();
    }

    /**
     * @return строковое представление текущего времени года
     */
    private String getLocalSeason() {
        String result = null;
        int monthValue = this.getZoneTime();

        if (Arrays.asList(12, 1, 2).contains(monthValue)) {
            result = "winter";
        } else if (Arrays.asList(3, 4, 5).contains(monthValue)) {
            result = "spring";
        } else if (Arrays.asList(6, 7, 8).contains(monthValue)) {
            result = "summer";
        } else if (Arrays.asList(9, 10, 11).contains(monthValue)) {
            result = "autumn";
        }

        return result;
    }

    /**
     * 0 — полнолуние.
     * 1-3 — убывающая Луна.
     * 4 — последняя четверть.
     * 5-7 — убывающая Луна.
     * 8 — новолуние.
     * 9-11 — растущая Луна.
     * 12 — первая четверть.
     * 13-15 — растущая Луна.
     * <p>
     * full-moon — полнолуние.
     * decreasing-moon — убывающая Луна.
     * last-quarter — последняя четверть.
     * new-moon — новолуние.
     * growing-moon — растущая Луна.
     * first-quarter — первая четверть.
     */
    private String getMoonPhaseName(int moonPhaseCode) {
        String result = null;

        if (moonPhaseCode == 0) {
            result = "full-moon";
        } else if (moonPhaseCode == 4) {
            result = "last-quarter";
        } else if (moonPhaseCode == 8) {
            result = "new-moon";
        } else if (moonPhaseCode == 12) {
            result = "first-quarter";
        } else if ((moonPhaseCode >= 1 && moonPhaseCode <= 3) || (moonPhaseCode >= 5 && moonPhaseCode <= 7)) {
            result = "decreasing-moon";
        } else if ((moonPhaseCode >= 9 && moonPhaseCode <= 11) || (moonPhaseCode >= 13 && moonPhaseCode <= 15)) {
            result = "growing-moon";
        }

        return result;
    }

    /**
     * Проверка статус кода
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenStatusCodeIs200() {
        Assert.assertEquals(200, this.response.getStatus());
    }

    /**
     * Проверка на получение ответа в формате json, если отсутсвует хедер Accept
     */
    @Test
    public void givenRequestWithNoAcceptHeader_WhenRequestIsExecuted_ThenBodyTypeIsJson() {
        Assert.assertTrue(this.response.getHeaders().get("Content-Type").contains("application/json; charset=utf-8"));
    }

    /**
     * lat - широта (в градусах) соответствует заданной вами;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenLatitudeIsCorrect() {
        Assert.assertThat(55.75396, is(this.json.getJSONObject("info").getDouble("lat")));
    }

    /**
     * lon - долгота (в градусах) соответствует заданной вами;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenLongitudeIsCorrect() {
        Assert.assertThat(37.620393, is(this.json.getJSONObject("info").getDouble("lon")));
    }

    /**
     * offset - проверьте часовой пояс;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenOffsetIsCorrect() {
        Assert.assertThat(10800, is(this.json.getJSONObject("info")
                .getJSONObject("tzinfo").getInt("offset")));
    }

    /**
     * name - проверьте название часового пояса;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenNameIsCorrect() {
        Assert.assertThat("Europe/Moscow", is(this.json.getJSONObject("info")
                .getJSONObject("tzinfo").getString("name")));
    }

    /**
     * abbr - проверьте сокращенное название часового пояса;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenAbbrIsCorrect() {
        Assert.assertThat("MSK", is(this.json.getJSONObject("info")
                .getJSONObject("tzinfo").getString("abbr")));
    }

    /**
     * dst - проверьте признак летнего времени;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenDstFieldIsCorrect() {
        Assert.assertThat(false, is(this.json.getJSONObject("info")
                .getJSONObject("tzinfo").getBoolean("dst")));
    }

    /**
     * url - проверьте страницу населенного пункта, убедитесь что ссылка правильная, что
     * широта и долгота внутри ссылки указаны верные;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenUrlContainsCorrectCoordinates() {
        Assert.assertTrue(this.json.getJSONObject("info").getString("url")
                .matches(".+lat.+55\\.75396.+lon.+37\\.620393"));
    }

    /**
     * season - проверьте сезон;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenSeasonIsCorrect() {
        Assert.assertThat(
                this.getLocalSeason(),
                is(this.json.getJSONObject("fact").getString("season")));
    }

    /**
     * Проверьте длину прогноза, убедитесь что прогноз действительно на два дня;
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenThereIsOnly2Forecasts() {
        Assert.assertThat(2, is(this.json.getJSONArray("forecasts").length()));
    }

    /**
     * Напишите логику и проверьте, что код фазы луны на второй день moon_code
     * соответсвует текстовому коду moon_text.
     */
    @Test
    public void givenCorrectLocation_WhenWeatherFor2DaysIsRetrieved_ThenMoonPhaseTextCorrespondsToCode() {
        JSONObject secondDayForecast = this.json.getJSONArray("forecasts").getJSONObject(1);
        Assert.assertThat(secondDayForecast.getString("moon_text"),
                is(this.getMoonPhaseName(secondDayForecast.getInt("moon_code"))));
    }
}
